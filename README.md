- How to run unit test
$gradle clean test

- How to build
$gradle clean build

- How to run app
Commandline: $gradle bootRun
Eclipse: Run main class org.shobhank.app.config.Application

- How to call apis
$curl -d '["http://www.aosabook.org/en/distsys.html","http://aosabook.org/en/index.html"]' -H "Content-Type: application/json" -X POST http://localhost:8080/xoom/submit
$curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/xoom/jobs/2

Sample response
{
  "jobId": 2,
  "status": {
    "http://aosabook.org/en/index.html": "5",
    "http://www.aosabook.org/en/distsys.html": "22"
  }
}

Sample response if jobid is invalid

{
    "errorCode": "Not Found",
    "errorMessage": "Requested job id not found, please check that the job id is correct"
}

- How to view swagger ui
http://localhost:8080/swagger-ui.html

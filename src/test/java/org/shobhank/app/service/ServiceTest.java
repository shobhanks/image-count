package org.shobhank.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.shobhank.app.dto.Job;

/**
 * Tests service layer
 * @author shosharma
 *
 */
// ASC order of name so that submit runs first then fetch
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(JUnit4.class)
public class ServiceTest {
    
    private static UrlProcessorService urlProcessor;
    
    @BeforeClass
    public static void setupContext() {
        urlProcessor = new UrlProcessorService();
    }
    
    /**
     * Tests if submitting returns job id
     */
    @Test
    public void firstTestSubmitAndReturn() {
        String urls[] = {"http://www.aosabook.org/en/distsys.html",
                "http://aosabook.org/en/index.html",
                "http://aosabook.org/en/git.html",
                "http://aosabook.org/en/yesod.html",
                "http://aosabook.org/en/oscar.html",
                "http://aosabook.org/en/posa/from-socialcalc-to-ethercalc.html",
                "http://aosabook.org/en/posa/ninja.html",
                "http://aosabook.org/en/posa/parsing-xml-at-the-speed-of-light.html",
                "http://aosabook.org/en/posa/memshrink.html"};
        List<String> urList = Arrays.asList( urls );
        int jobId = urlProcessor.submitAndReturn( urList );
        assertNotNull( jobId );
        assertEquals( jobId, 1 );
    }
    
    /**
     * Runs after submitting the job, alphabetically
     * Check pending and done status
     * @throws InterruptedException
     */
    @Test
    public void nextTestGet() throws InterruptedException {
        Job job = urlProcessor.getJob( 1 );
        assertNotNull( job );
        assertTrue( 1 == job.getJobId() );
        Map<String, String> urlStatus = job.getUrlToStatus();
        Set<String> status = new HashSet<>( urlStatus.values() );
        assertTrue( status.contains( "Pending" ) );
        
        Thread.sleep( 30000 );
        job = urlProcessor.getJob( 1 );
        urlStatus = job.getUrlToStatus();
        status = new HashSet<>( urlStatus.values() );
        assertFalse( status.contains( "Pending" ) );
    }
        
    
}

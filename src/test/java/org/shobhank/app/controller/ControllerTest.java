package org.shobhank.app.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.shobhank.app.controller.PageParseController;
import org.shobhank.app.dto.Job;
import org.shobhank.app.service.UrlProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * Testing Controller/Resource layer
 * @author shosharma
 *
 */
@ContextConfiguration
@RunWith(SpringRunner.class)
@WebMvcTest(value=PageParseController.class, secure=false)
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    ObjectMapper objectMapper;
    
    @MockBean
    private UrlProcessorService urlProcessor;
    
    /**
     * Create mocks
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setupContext() {
        Mockito.when( urlProcessor.submitAndReturn( Mockito.anyList() ) ).thenReturn( 1 );
        Job job = new Job();
        job.setJobId( 1 );
        Map<String, String> urlToStatus = new HashMap<>();
        urlToStatus.put( "url1", "Pending" );
        urlToStatus.put( "url2", "5" );
        job.setUrlToStatus( urlToStatus );
        Mockito.when( urlProcessor.getJob( 1 ) ).thenReturn( job );
        Mockito.when( urlProcessor.getJob( 99 ) ).thenCallRealMethod();
    }
    
    /**
     * Test post api
     * @throws JsonProcessingException
     * @throws Exception
     */
    @Test
    public void postJob() throws JsonProcessingException, Exception {
        List<String> urList = new ArrayList<>();
        urList.add( "http://google.com" );
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/xoom/submit" )
                .contentType( MediaType.APPLICATION_JSON )
                .content( objectMapper.writeValueAsString( urList ) );
        MvcResult result = mockMvc.perform( requestBuilder ).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals( HttpStatus.OK.value(), response.getStatus() );
        assertNotNull( response.getContentAsString() );
        assertEquals( response.getContentAsString(), "1" );
    }
    
    /**
     * Tests get job with mocks
     * @throws JsonProcessingException
     * @throws Exception
     */
    @Test
    public void getJob() throws JsonProcessingException, Exception {        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get( "/xoom/jobs/" + 1 )
                .contentType( MediaType.APPLICATION_JSON );
        MvcResult result = mockMvc.perform( requestBuilder ).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals( HttpStatus.OK.value(), response.getStatus() );
        assertNotNull( response.getContentAsString() );
        assertEquals( response.getContentAsString(), "{\"jobId\":1,\"status\":{\"url1\":\"Pending\",\"url2\":\"5\"}}" );
    }
    
    /**
     * Test get job negative scenario.
     * Tests not found exception
     * @throws JsonProcessingException
     * @throws Exception
     */
    @Test
    public void getJobNotFound() throws JsonProcessingException, Exception {        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get( "/jobs/" + 99 )
                .contentType( MediaType.APPLICATION_JSON );
        MvcResult result = mockMvc.perform( requestBuilder ).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals( HttpStatus.NOT_FOUND.value(), response.getStatus() );
    }
    
    @Configuration
    @ComponentScan("org.shobhank.app")
    static class Config {
        
    }
}

package org.shobhank.app.controller;

import java.util.List;
import org.apache.log4j.Logger;
import org.shobhank.app.dto.Job;
import org.shobhank.app.service.UrlProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * Main controller for submit job and get job status api.
 * Should be very light in code as major business logic should stay in Service layer
 * @author shosharma
 *
 */
@Api
@RequestMapping(value="/xoom")
@RestController
public class PageParseController {

    static final Logger logger = Logger.getLogger( PageParseController.class );
    
    @Autowired
    private UrlProcessorService urlProcessor;
    
    /**
     * Submits the job asynchronously and returns the jobid
     * @param urls
     * @return
     */
    @ApiOperation(value = "Submits job asynchronously and returns job id which can be used for getting job status in future")
    @ApiResponses(value = @ApiResponse(code=200,message="OK"))
    @RequestMapping(value="/submit", method=RequestMethod.POST)
    public Integer parseUrls(@RequestBody List<String> urls) {
        return urlProcessor.submitAndReturn( urls );
    }
    
    /**
     * Get information about submitted job. 
     * @param jobId
     * @return
     */
    @ApiOperation(value = "Gives status of job detailing the parsed(or pending) urls with count of images discovered")
    @ApiResponses(value = {@ApiResponse(code=200,message="Jobs"),
            @ApiResponse(code=404, message="Requested job id not found, please check that the job id is correct")})
    @RequestMapping(value="/jobs/{id}", method=RequestMethod.GET)
    public Job getJobStatus(@PathVariable("id") int jobId) {
        return urlProcessor.getJob( jobId );
    }
       
    
}

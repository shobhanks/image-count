package org.shobhank.app.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

/**
 * Thread implementation that does the actual GET url call and parses the response.
 * Instance of this class will be submitted to threadpool
 * @author shosharma
 *
 */
public class ParseTask implements Callable<Integer>{
    
    private static final Logger logger = Logger.getLogger( ParseTask.class );
    private static final String IMAGE_REGEX = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
    private final String url;
    
    public ParseTask(String url) {
        this.url = url;
    }
    
    @Override
    public Integer call() throws Exception {
        logger.debug( "Processing URL " + url);
        int count = countImages( url );
        logger.debug( "Processed URL " + url + " Found " + count + " Images");
        return count;
    }
    
    private int countImages(String url) {
        String response = "";
        try {
            response = executeGet( url );
        } catch ( IOException e ) {
            logger.error( e.getMessage() );
        }
        Pattern p = Pattern.compile(IMAGE_REGEX);
        Matcher matcher = p.matcher(response);
        int count = 0;
        while(matcher.find()) {
            count++;
        }
        return count;
    }
    
    private String executeGet(String url) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        StringBuffer response = new StringBuffer();
        try {
            Thread.sleep( 1000 );
        } catch ( InterruptedException e ) {
            logger.error( e.getMessage() );
        }
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } 
        // if not success then also we return as count images will be zero. 
        // This can easily be changed to support for bad or invalid urls
        return response.toString();
    }
}

package org.shobhank.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.shobhank.app.dto.Job;
import org.shobhank.app.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Service layer processor, submits job, creates threads and give to the executor service
 * @author shosharma
 *
 */
@Service
public class UrlProcessorService {
    
    private static int jobIdSequence= 0;
    
    private final int MAX_POOL_SIZE = 10;
    
    static final Logger logger = Logger.getLogger( UrlProcessorService.class );
    
    // This map maintains job status once job is submitted.
    // It is used later to fetch the job and individual url status
    private static Map<Integer, Map<String, Future<Integer>>> jobToUrls = new HashMap<>(); 
        
    /**
     * This will be called by resource layer to submit job
     * @param urls
     * @return
     */
    public int submitAndReturn(List<String> urls) {
        synchronized( this ) {
            jobIdSequence++;
        }
        process( jobIdSequence, urls );
        return jobIdSequence;
    }
    
    private void process(int jobId, List<String> urls) {
        // Create threadpool
        ExecutorService executorService = Executors.newFixedThreadPool( MAX_POOL_SIZE );
        Map<String, Future<Integer>> urlToFuture = new HashMap<>();
        
        // create individual threads and submit to the executor service
        for(String url: urls) {
            Callable<Integer> parseTask = new ParseTask( url );
            Future<Integer> future = executorService.submit( parseTask );
            urlToFuture.put( url, future );
        }
        synchronized( this ) {
            jobToUrls.put( jobId, urlToFuture );
        }
        
        executorService.shutdown();
    }
    
    /**
     * This is called by resource layer to fetch job status by job id
     * @param jobId
     * @return
     */
    public Job getJob(int jobId) {
        // get individual job status
        Map<String, Future<Integer>> urlToFuture = getJobToUrls().get( jobId );
        if(urlToFuture==null || urlToFuture.isEmpty())
            throw new ResourceNotFoundException( jobId, "Requested job id not found, please check that the job id is correct" );
        // create job dto
        Job job = new Job();
        job.setJobId( jobId );
        Map<String, String> urlStatus = new HashMap<>();
        for(String url: urlToFuture.keySet()) {
            Future<Integer> future = urlToFuture.get( url );
            try {
                urlStatus.put( url,  (future.isDone()?future.get()+"":"Pending"));
            } catch ( InterruptedException | ExecutionException e ) {
                logger.error( e.getMessage() );
            }
        }
        job.setUrlToStatus( urlStatus );
        return job;
    }
    
    private Map<Integer, Map<String, Future<Integer>>> getJobToUrls() {
        return jobToUrls;
    }
}

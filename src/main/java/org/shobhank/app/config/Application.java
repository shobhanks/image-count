package org.shobhank.app.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class creates swagger configuration and is also the main class for Spring boot application
 * @author shosharma
 *
 */

@EnableSwagger2
@ComponentScan("org.shobhank.app")
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public Docket petApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName( "xoom-api" )
                .apiInfo( apiInfo() )
                .select()
                .apis( RequestHandlerSelectors.withClassAnnotation( Api.class ) )
                .build();
    }
    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title( "Image Count REST Api" )
                .description( "Java Image count api reference for developers" )
                .contact( "shobhanks1987@gmail.com" )
                .version( "0.0.1" )
                .build();
    }
}

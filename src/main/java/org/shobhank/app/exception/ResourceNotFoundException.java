package org.shobhank.app.exception;

/**
 * 
 * @author shosharma
 *
 */
public class ResourceNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;
    private int resourceId;
    
    public ResourceNotFoundException(int resourceId, String message) {
        super(message);
        this.setResourceId( resourceId );
    }
    
    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId( int resourceId ) {
        this.resourceId = resourceId;
    }
}

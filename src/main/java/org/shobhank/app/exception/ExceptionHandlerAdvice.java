package org.shobhank.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Exception handler. Can be extended for other exceptions in future like Forbidden or Internal Server if we start using database
 * @author shosharma
 *
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException resourceNotFoundException){
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode( "Not Found" );
        response.setErrorMessage( resourceNotFoundException.getMessage() );
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
    }
}

package org.shobhank.app.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Job DTO
 * @author shosharma
 *
 */
public class Job {
    private Integer jobId;
    
    @JsonProperty("status")
    private Map<String, String> urlToStatus;
    
    public Integer getJobId() {
        return jobId;
    }
    public void setJobId( Integer jobId ) {
        this.jobId = jobId;
    }
    public Map<String, String> getUrlToStatus() {
        return urlToStatus;
    }
    public void setUrlToStatus( Map<String, String> urlToStatus ) {
        this.urlToStatus = urlToStatus;
    }
    
}
